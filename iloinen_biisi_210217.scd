(
SynthDef(\bass,{
	arg freq = 100,
	rel = 1,
	filFreq = 2000,
	amp = 0.3,
	noiseFreq = 150,
	out = 0;
	var ampEnv, sig, filter;
	ampEnv = EnvGen.kr(Env.new([0,1,0], [0.03,rel], -3), doneAction: 2);
	sig = Pulse.ar(freq) + LFNoise0.ar(noiseFreq, 0.1);
	filter = LPF.ar(sig, filFreq, ampEnv*amp)!2;
	Out.ar(out, filter);
}).add;
)

(
SynthDef(\harmonikka,{
	arg freq = 300,
	att = 0.05,
	sus = 0.5,
	rel= 0.1,
	filFreq = 15000,
	sinAmp = 0.6,
	sawAmp = 0.4,
	amp = 0.1,
	out = 0;
	var ampEnv, saw, sin, filter;
	ampEnv = EnvGen.kr(Env.new([0,1,1,0], [att,sus,rel]), doneAction: 2);
	saw = Saw.ar(freq, sawAmp);
	sin = SinOsc.ar(freq * 2, 0, sinAmp);
	filter = LPF.ar(saw + sin, filFreq, ampEnv*amp)!2;
	Out.ar(out, filter);
}).add;
)

(
SynthDef(\trumpet,{
	arg freq=300, att=0.04, sus=0.7, rel=0.2,
	amp=0.4, pulseAmp=1, sawAmp=0.5, filFreq = 2000;
	var ampEnv, filCurve, filter, sig;
	ampEnv = EnvGen.kr(Env.new([0,1,1,0], [att,sus,rel], [0,0,3]), doneAction:2);
	filCurve = SinOsc.kr(50,0, Line.kr(filFreq, 0.001,0.5), filFreq);
	sig = Pulse.ar(freq, 0.5,pulseAmp) + Saw.ar(freq, sawAmp);
	filter = LPF.ar
	(sig, filCurve, ampEnv*amp)!2;
	Out.ar(0, filter);
}).add;
)

(
Pdef(\ila,
	Pbind(
		\instrument, \bass,
		\dur, Pseq([3/4,1/4,1], inf),
		\freq, Pseq([1,1,5/4,1,1,15/16]*100, inf),
		\rel, 0.3,
		\amp, 0.4,
		\filFreq, 400,
		\noiseFreq, Pseq([Pwhite(90,110,2),150, Pwhite(90,110,2),50], inf)
	)
).play(quant:4);
//quant_(4);
)

(
Pdef(\ilb,
	Pbind(
		\instrument, \harmonikka,
		\dur, 1/4,
		\rel, Pseq([0.3] ++ (0.1!3), inf),
		\sus, 0.02,
		\freq, Pseq([
			Pseq([4/3,1,1,1, 5/4,1,1,1], 2),
			Pseq([4/3,1,1,1, 3/2,1,1,1], 1),
			Pseq([4/3,1,1,1, 5/4,1,1,1], 1) ]*800, inf),
//		\freq, Pseq([4/3,5/4, 4/3,5/4, 4/3,3/2, 4/3,5/4]*400, inf),
		\amp, Pseq([0.1, Pwhite(0.03,0.06,3)], inf),
	)
).play(quant:4);
//quant_(4);
)
Pdef(\ilb).stop;

(
Pdef(\ilc,
	Pbind(
		\instrument, \trumpet,
		\freq, Pseq([5/4,3/2]*400, inf),
//		\freq, Pseq([3/2,5/3,3/2, 5/4,4/3,5/4, 1,9/8,1,  5/6,3/4]*400, inf),
		\sus, Pwhite(0.1,0.5),
		\rel, Pwhite(2,3),
//		\rel, Pseq([3/16,0.1,1/2, 3/16,0.1,1/2, 3/16,0.1,1/2, 1/2,3],inf),
		\filFreq, 1000,
		\dur, 4,
//		\dur, Pseq([3/8,1/8,1, 3/8,1/8,1/2, 3/8,1/8,1/2, 1/2,4], inf),
		\att, 0.0001
	)
).//play(quant:4);
quant_(4);
)
Pdef(\ilc).stop;


// Copyright CC-BY-SA Ilkka Virkunen 2017
// Creative Commons Attribution-ShareAlike 4.0 International Licence
// http://creativecommons.org/licenses/by-sa/4.0/
// Feel free to share and remix!
