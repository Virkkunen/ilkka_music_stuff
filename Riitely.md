#Riiteleminen

On vanha vitsi että pariskunnat riitelevät paljon. Riitelemistä pidetään melko normaalina sosiaalisena ilmiönä parisuhteissa, jopa tarpeellisena kommunikointikeinona.

En usko että riiteleminen on koskaan varsinaisesti hyödyllistä, tai ainakaan tehokas tapa kommunikoida. Olen itse elänyt suurimman osan elämääni yksikseni, mutta päädyttyäni viimein jonkinasteiseen parisuhteeseen olen katsonut tärkeäksi ymmärtää riitelemisen syitä ja miettiä miten sitä voi ehkäistä.

##Protokollaerot

Miehet ja naiset toimivat sosialisissa tilanteissa ratkaisevasti eri tavalla. Miesten keskinäinen ystävyys perustuu ongelmanratkaisuun, kun taas naisten välinen ystävyys kommunikointiin. Ongelmia syntyy kun mies puhuu puolisolleen (naiselle) käyttäen miesprookollaa ja päinvastoin, sillä nämä tähtäävät ratkaisevasti eri asioihin. Käyn näitä kahta protokollaa ensin läpi vähän tarkemmin.

###Miesprotokolla

Kun tavataan miesporukassa, homma menee yleensä niin, että joku ehdottaa että tehdään jotain, ja muut joko lähtevät mukaan tai sitten eivät. Se tartutaanko tarjoukseen riippuu siitä, vaikuttaako ehdotus siltä, että siitä on osallistujille hyötyä. Jos ehdotukseen tartutaan, nousee ehdottaja porukan _johtajaksi_. Johtaja saattaa vaihtua kesken ongelmanratkaisuprosessin, jos joku muu porukasta osoittaa olevansa kykenevämpi tai tietäväisempi johtaja, tai siksi että hän osaa _vaikuttaa_ todella karismaattiselta yms.

Ongelma tai tekeminen voi olla mitä tahansa, mistä on kaikille osallistujille hyötyä, esimerkiksi lavuaarin korjaamista, naisten iskemistä tai pohdintaa siitä pitäisikö syödä punajuuri- vai linssikeittoa. Johtajaa kunnioitetaan, koska hän on johtaja-asemassaan ongelmanratkaisun tärkein palanen --- ja kaikkihan haluavat ongelman ratkeavan. Toisaalta kaikki haluavat johtajaksi, koska toisten kunnioitus tuntuu hyvältä, mutta normaaleilla ihmisillä nämä kaksi tavoitetta johtavat samaan toimintaan: pyrkimykseen tuoda esille aina kaikista relevanteimman informaation ongelmanratkaisun kannalta. Näin ollen johtajaksi valikoituu vuorostaan aina se, jolla on eniten hyödyllistä annettavaa ongelmanratkaisun kannalta.

Tarkastellaanpa tätä protokollaa vielä hieman lähemmin keskustelun osalta. Keskustelu on ryhmän ajatustenvaihtoa ja tiedottamista. Jos joku sanoo jotain hyödyllistä, on järkevä reaktio tiedostaa tämä, ja osoittaa jonkinlaista kunnioitusta. Tällöin kaikki ryhmän jäsenet saavat hyödyllisen tiedon käyttöönsä, ja sanoja saa kunnioituksesta pienen hyvänolon tunteen, joka kannustaa häntä sanomaan vastaisuudessakin hyviä ideoita ääneen. Vastaavasti hölmöjä ideoita ei tietenkään kannata samalla tavalla tukea.

Kuitenkaan sanojan sanojen varsinainen _sisältö_ ei ole ainoa määräävä tekijä kunnioituksen antamisessa, vaan myös äänensävy, kehonkieli ja keskusteluhistoria vaikuttaa tähän. Kovaan ääneen painokkaasti sanottu ajatus saa enemmän kannatusta kuin sama ajatus sanottuna epävarmasti ja hiljaa. Kehonkieli, äänenkorkeus yms viestivät vastaavasti. Myös jos henkilö on keskustelun aikana sanonut jo monta tosi hyvää ajatusta, on häntä helppo tukea jatkossakin.

Normaalilla ihmisellä tämä _vakuuttavuus_ määräytyy tavallisesti sen mukaan kuinka hyvänä pitää omaa ideaansa, joten luonnostaan hyvät ideat tulee sanottua vakuuttavampaan sävyyn ja homma toimii. Tätä järjestelmää on kuitenkin mahdollista huijata sanomalla jotakin triviaalia tai tyhmää _todella_ vakuuttavasti, ja saada ihmiset seuraamaan sinua. Esimerkiksi Donald Trump ei melkein koskaan _sano_ mitään.

Järkevä ihminen pyrkii sitten varsinkin tuntemattomassa seurassa tunnistamaan huijareita, ja kiinnittää huomiota nimenomaan keskustelukumppanin _asiasisältöön_, eikä osoita kunnioitusta ennen kuin on tullut varmaksi siitä, että toisella on oikeasti jotakin järkevää sanottavaa. Varsinkin hän suhtautuu varauksellisesti karismaattiseen, mutta epäselvään tai triviaaliin ulosantiin.

Miesprotokollassa on siis kyse järkevästä ongelmanratkaisusta.

###Naisprotokolla

Naisprotokollaan minulla ei tietysti voi olla kuin toisen käden tietoa, jota oma miesajatteluni vääristää, mutta luullakseni siinä on kyse _samanlaisuudentunteesta_. Tietysti naisetkin vaihtavat tietoa, ja paljon, mutta keskustelunaloitus on paljon hienovaraisempi kuin miehillä. Tyypillisessä naisprotokollatilanteessa on kaksi naista, ja kaikki alkaa kommunikointiyhteyden luomisesta.

Keskustelun alkaessa osapuolet varmistuvat siitä, että he keskittyvät vain toisiinsa. Osapuolet katsovat toisiaan silmiin, hymyilevät, nyökkäilevät ja myötäilevät toisen sanomisia. Jos jompikumpi oli tekemässä jotain, hän lopettaa sen, ainakin keskustelunavauksen ajaksi. Jos tapaaminen oli ennalta sovittu, ei kumpikaan tee mitään koko keskustelun aikana.

Keskustelun avauksen aikana keskustelun denotatiivisellä merkityksellä ei ole paljoakaan väliä. _Yhteyden luominen_ on kaikki kaikessa. Myös keskustelun jo ollessa käynnissä yhteyttä täytyy jatkuvasti pitää yllä. Osapuolet keskittyvät pelkästään toisiinsa. Jos jompikumpi alkaa vilkuilla muualle tai tekemään jotain muuta, on se (epäkohteliasta, ja) merkki siitä että hän haluaa lopettaa keskustelun.

Keskustelun ylläpitäminen vaatii tietyssä mielessä ''jatkuvaa työtä''. Tätä työtä on katsekontaktin ja myötäilyn lisäksi esimerkiksi näennäisesti merkityksetön triviatieto toisesta ihmisestä, kuten esimerkiksi opiskeluhistoria, perhesuhteet, lempiruuat tai aurinkomerkit. Mitä enemmän ja mitä pikkutarkempia asioita toisesta tietää, sitä voimakkaammin se viestii ystävyydestä ja välittämisestä.

###Päiväkirjamerkintä

Anette saapui kotiin, oli valmiiksi vähän vituuntuneen oloinen, tosin ehkä vain väsynyt. Ei vastannut heti kun sanoin moi, mutta vastasi kyllä sitten kun saapui niinkun samaan tilanteeseen mun kanssa.

Anette alkoi kertoa päivästään. Keskityin samalla tietokoneeseen, ja Anette aika lailla saman tien huomautti, etä se on ärsyttävää ja häiritsevää. Minä reagoin uhmakkaasti, enkä laittanut tietokonetta pois, mutta kuuntelin kuitenkin parhaani mukaan mitä Anette kertoi.

Minulle tuli olo että Anette vaati minua alistumaan tahtoonsa. Todellisuudessa hän varmaankin vain halusi jotenkin luoda sosiaalisen kontekstin, jossa osapuolet voivat olla varmoja siitä, että toinen kuuntelee. Itse tulkitsin sen jonkinlaiseksi valtamäärittelyksi, jossa Anette halusi alistaa minut toimimaan oman tahtonsa mukaan.

##Ristiriidat

Nais- ja miesprotokollat ovat  molemmat sinällään todella hyödyllisiä ja toimivia omilla _pätevyysalueillaan_, mutta keskenään ne ovat sopivat kehnosti yhteen, varsinkin jos kommunikoitavan informaation määrä on suuri, kuten esimerkiksi parisuhteissa. Mies ei ymmärrä ettei nainen oikeasti yritä ratkaista mitään ongelmaa, vaikka hän voi siltä alunalkuun vaikuttaa; hän vain yrittää ystävystyä miehensä kanssa.

Vsataavasti mies yrittää ystävystyä puolisonsa kanssa ratkaisemalla tämän ongelmia, tai tarjoamalla omia ongelmiaan ratkottavaksi. Hän haluaa naisen kertovan hänelle tarkalleen mitä tämän päässä liikkuu, ja pohtia yhdessä ratkaisuvaihtoehtoja. Joka varmaan onnistuisi, jos nainen ymmärtäisi pyynnön.

Kaikki tietävät että naiset ja miehet ajattelevat eri tavalla, mutta koska ero on niin perustavanlaatuinen, on samaistuminen toisen ajattelumalliin todella hankalaa. Kukaan ihminen ei voi _päättää_ alkaa ajatella eri tavalla kuin mitä ajattelee. Tällöin vaikka olisi tietoinen ajattelueroista, ei tietoa osaa välttämättä käyttää hyväksi riitatilanteessa. Toisaalta riitatilanne on usein emotionaalinen, ja tunnemyrsky haittaa ajattelua ja samaistumiskykyä entisestään.

##Muut syyt

Omat ongelmat, yleinen vitutus, ahdistus, häpeä. Vaarallisimpia tunteita ovat ne, joista ei kehtaa puhua, ja ne joita ei ymmärrä.


